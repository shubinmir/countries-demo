<?php namespace Developer\Countries;

use Backend;
use System\Classes\PluginBase;

/**
 * countries Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'countries',
            'description' => 'No description provided yet...',
            'author'      => 'developer',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Developer\Countries\Components\Countries' => 'Countries',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'developer.countries.some_permission' => [
                'tab' => 'countries',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'countries' => [
                'label'       => trans('developer.countries::lang.countries'),
                'url'         => Backend::url('developer/countries/countries'),
                'icon'        => 'icon-globe',
                'permissions' => ['developer.countries.*'],
                'order'       => 500,
            ],
        ];
    }
}
