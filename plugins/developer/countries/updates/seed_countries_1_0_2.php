<?php namespace Igvs\Courses\Updates;

use Developer\Countries\Models\Country;
use October\Rain\Database\Updates\Seeder;

class SeedCountries_1_0_2 extends Seeder
{
    public function run()
    {
        foreach($this->getCountryRow() as $arr) {
            Country::insert([
                'name_en' => $arr['name_en'],
                'name_ru' => $arr['name_ru'],
                'code'    => $arr['code'],
                'days'    => rand(1, 100),
            ]);
        }
    }

    private function getCountryRow()
    {
        $data_path = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'data/countries.csv';

        $handle = fopen($data_path, 'r');

        while($row = fgets($handle)) {
            $row = explode(';', $row);

            if (!count($row)) {
                return false;
            }

            yield [
                'name_en' => $row[2],
                'name_ru' => $row[1],
                'code'    => $row[0],
            ];
        }

        fclose($handle);

        return false;
    }
}
