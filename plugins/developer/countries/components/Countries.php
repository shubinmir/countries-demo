<?php namespace Developer\Countries\Components;

use Cms\Classes\ComponentBase;
use Developer\Countries\Models\Country;
use Flash;
use Input;

class Countries extends ComponentBase
{
    use \Developer\Countries\Traits\Countries;

    public function componentDetails()
    {
        return [
            'name'        => 'Countries Component',
            'description' => 'World map about popular ways of tourism.'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->addJs('components/countries/resources/am-charts/js/core.js');
        $this->addJs('components/countries/resources/am-charts/js/maps.js');
        $this->addJs('components/countries/resources/am-charts/js/animated.js');
        $this->addJs('components/countries/resources/am-charts/js/worldLow.js');
        $this->addJs('components/countries/resources/am-charts/js/RU.js');
        $this->addJs('components/countries/assets/js/map.js');

        $this->addCss('components/countries/assets/css/map.css');
    }

    public function onRender()
    {
        $this->page['countries_list'] = $this->getCountriesList();
        $this->page['days_data'] = $this->getCountriesDays();
    }

    /**
     * Ajax method for add days to selected country
     */
    public function onAddDays(): array
    {
        $count_days = (int) Input::get('days', 0);
        $country_code = Input::get('country', '');

        $add_result = $this->addDaysToCountry($country_code, $count_days);

        if ($add_result) {
            Flash::success('Вы добавили свой опыт.');
        } else {
            Flash::error('Опыт не сохранился. Что-то пошло не так :(');
        }

        $this->onRender();

        return [
            '#days-data' => $this->renderPartial('@days-data-ajax')
        ];
    }

    /**
     * @param $code string Country's code
     * @param $days int Days
     * @return bool
     */
    private function addDaysToCountry(string $code, int $days): bool
    {
        $country = Country::where('code', $code)->first();

        if (!$country) {
            return false;
        }

        $country->days += $days;
        $country->save();

        return true;
    }
}
