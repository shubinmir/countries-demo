am4core.useTheme(am4themes_animated);

var chart = am4core.create("chartdiv", am4maps.MapChart);


try {
    chart.geodata = am4geodata_worldLow;
    chart.geodataNames = am4geodata_lang_RU;
}
catch (e) {
    chart.raiseCriticalError(new Error("Map geodata could not be loaded. Please download the latest <a href=\"https://www.amcharts.com/download/download-v4/\">amcharts geodata</a> and extract its contents into the same directory as your amCharts files."));
}


chart.projection = new am4maps.projections.Miller();

var title = chart.chartContainer.createChild(am4core.Label);
title.text = "Популярные места отдыха в Мире";
title.fontSize = 20;
title.paddingTop = 30;
title.align = "center";

var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
var polygonTemplate = polygonSeries.mapPolygons.template;
polygonTemplate.tooltipText = "{name}: {value.value} дней";
polygonSeries.useGeodata = true;
polygonSeries.heatRules.push({ property: "fill", target: polygonSeries.mapPolygons.template, min: am4core.color("#dcdcdc"), max: am4core.color("#AAAA00") });


// add heat legend
var heatLegend = chart.chartContainer.createChild(am4maps.HeatLegend);
heatLegend.valign = "bottom";
heatLegend.series = polygonSeries;
heatLegend.width = am4core.percent(100);
heatLegend.orientation = "horizontal";
heatLegend.padding(30, 30, 30, 30);
heatLegend.valueAxis.renderer.labels.template.fontSize = 10;
heatLegend.valueAxis.renderer.minGridDistance = 40;

polygonSeries.mapPolygons.template.events.on("over", function (event) {
    handleHover(event.target);
})

polygonSeries.mapPolygons.template.events.on("hit", function (event) {
    handleHover(event.target);
})

function handleHover(mapPolygon) {
    if (!isNaN(mapPolygon.dataItem.value)) {
        heatLegend.valueAxis.showTooltipAt(mapPolygon.dataItem.value)
    }
    else {
        heatLegend.valueAxis.hideTooltip();
    }
}

polygonSeries.mapPolygons.template.events.on("out", function (event) {
    heatLegend.valueAxis.hideTooltip();
})



// excludes Antarctica
polygonSeries.exclude = ["AQ"];
