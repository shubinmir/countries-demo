<?php namespace Developer\Countries\Traits;


use Developer\Countries\Models\Country;

trait Countries
{
    /**
     * Return country list
     * @return array country list with code at key and russian name at value
     */
    private function getCountriesList(): array
    {
        return Country::orderBy('name_ru')->lists('name_ru', 'code');
    }

    /**
     * Return days of countries
     * @return array days of country with code at key and day at value
     */
    private function getCountriesDays()
    {
        return Country::select('code as id', 'days as value')->get();
    }
}
