<?php

return [
    'countries' => 'Countries',
    'country'   => 'Country',
    'columns'   => [
        'countries' => [
            'code'    => 'Code',
            'name_ru' => 'Russian Name',
            'name_en' => 'English Name',
            'days'    => 'Count Days',
        ]
    ],
    'form' => [
        'preview'          => 'Preview',
        'create'           => 'Create',
        'save'             => 'Save',
        'create_and_close' => 'Create and Close',
        'save_and_close'   => 'Save and Close',
        'cancel'           => 'Cancel',
        'delete'           => 'Delete',
        'return_to_list'   => 'Return to list',
        'or'               => 'or',
        'creating'         => 'Creating',
        'editing'          => 'Editing',
        'saving'           => 'Saving',

        'delete_ask'       => 'Are you sure you want to delete the selected Countries?',
    ],
];
